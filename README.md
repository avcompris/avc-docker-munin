# avc-docker-munin

Docker image: avcompris/munin

Usage:

	$ docker run -d --name munin \
		-e CRON_DELAY=2 \
		-e THIS_NODE_NAME=sample-munin.avcompris.com \
	    -v /data/munin/db:/var/lib/munin \
	    -v /data/munin/logs:/var/log/munin \
	    -v /data/munin/cache:/var/cache/munin \
	    -e SSH_TUNNEL_USER=infra \
	    -e SSH_TUNNEL_HOST=proxy.avcompris.com \
	    avcompris/munin


Exposed port is 80.

CRON_DELAY is 5 by default (That is, 5 minutes).

See also:
[https://github.com/occitech/docker/tree/master/munin](https://github.com/occitech/docker/tree/master/munin)
