# File: avc-docker-munin/Dockerfile
#
# Use to build the image: avcompris/munin

FROM nginx:1.17.8
MAINTAINER david.andriana@avantage-compris.com

#-------------------------------------------------------------------------------
#   1. DEBIAN PACKAGES
#-------------------------------------------------------------------------------

RUN apt-get update

# autossh is used to keep SSH tunnels persistent.
# netcat is used for troubleshooting (example: Run "nc 127.0.0.1 4949")
#
RUN apt-get install -y \
	ntp \
	munin \
	netcat \
	autossh \
	ssh-client

#-------------------------------------------------------------------------------
#   2. NGINX CONFIG
#-------------------------------------------------------------------------------

RUN rm /etc/nginx/conf.d/default.conf

COPY nginx_munin.conf /etc/nginx/conf.d/

#-------------------------------------------------------------------------------
#   3. SSH CONFIG
#-------------------------------------------------------------------------------

RUN mkdir /root/.ssh

RUN chmod 700 /root/.ssh

COPY ssh_config /root/.ssh/config

RUN chmod 600 /root/.ssh/config

#-------------------------------------------------------------------------------
#   4. MUNIN PLUGINS
#-------------------------------------------------------------------------------

RUN ln -s /usr/share/munin/plugins/munin_update /etc/munin/plugins/

#-------------------------------------------------------------------------------
#   7. SCRIPTS
#-------------------------------------------------------------------------------

COPY entrypoint.sh .

#-------------------------------------------------------------------------------
# 8. BUILDINFO (RELY ON JENKINS: POPULATED VIA "buildinfo.sh")
#-------------------------------------------------------------------------------

COPY buildinfo /

#-------------------------------------------------------------------------------
#   9. END
#-------------------------------------------------------------------------------

EXPOSE 80

CMD [ "/bin/bash", "entrypoint.sh" ]
