#/bin/bash

# File: avc-docker-munin/entrypoint.sh
#
# See also: https://github.com/occitech/docker/blob/master/munin/run.sh

set -e

mkdir -p /var/cache/munin/www
mkdir -p /var/lib/munin/cgi-tmp

# ensure munin fle have right permission
chown -R munin:munin /var/cache/munin
chown -R munin:munin /var/lib/munin
chmod -R ugo+rw /var/lib/munin/cgi-tmp

CRON_DELAY="${CRON_DELAY:=5}"
sed -i "s/\*\/5/\*\/${CRON_DELAY}/g" /etc/cron.d/munin

THIS_NODE_NAME="${THIS_NODE_NAME:=munin}"
sed -i "s/^\[localhost\.localdomain\]/\[${THIS_NODE_NAME}\]/g" /etc/munin/munin.conf
# sed -i "s/use_node_name yes/use_node_name no/" /etc/munin/munin.conf
sed -i "s/^\s*#\?\s*host_name .*/host_name ${THIS_NODE_NAME}/g" /etc/munin/munin-node.conf

if [ -n "${SSH_TUNNEL_USER}" ]; then
	if [ -z "${SSH_TUNNEL_HOST}" ]; then
		echo "SSH_TUNNEL_HOST should be set (SSH_TUNNEL_USER=${SSH_TUNNEL_USER})" >&2
		echo "Exiting." >&2
		exit 1
	fi
	sed -i "s/SSH_TUNNEL_USER/${SSH_TUNNEL_USER}/g" /root/.ssh/config
	sed -i "s/SSH_TUNNEL_HOST/${SSH_TUNNEL_HOST}/g" /root/.ssh/config

	SSH_TUNNEL_KEY=/etc/ssl/private/munin/munin.key

	if [ ! -f "${SSH_TUNNEL_KEY}" ]; then
		echo "Private key should exist: ${SSH_TUNNEL_KEY}" >&2
		echo "Exiting." >&2
		exit 1
	fi

	find /etc/munin/munin-conf.d -type f | while read i; do
		if grep -q "^\s*address\s\s*127.0.0.1" "${i}" && grep -q "^\s*port\s" "${i}"; then
			port=`grep "^\s*port\s" "${i}" | awk '{print $2}'`
			if [ -z "${port}" ]; then
				echo "Error: Cannot extract port from: ${i}" >&2
				echo "Exiting." >&2
				exit 1
			fi
			echo "Creating SSH tunnel with: ${SSH_TUNNEL_USER}@${SSH_TUNNEL_HOST}:${port}..."
			autossh -M 0 -N -f -L "${port}:localhost:${port}" ssh_proxy
		fi
	done
fi

# placeholder html to prevent permission error
if [ ! -f /var/cache/munin/www/index.html ]; then
	cat << EOF > /var/cache/munin/www/index.html
<html>
  <head>
    <title>Munin</title>
  </head>
  <body>
    Munin has not run yet.  Please try again in a few moments.
  </body>
</html>
EOF
	chown -R munin:munin /var/cache/munin/www/index.html
fi

# start local munin-node
echo "Starting munin-node: ${THIS_NODE_NAME}..."
/etc/init.d/munin-node start

# start cron
echo "Starting munin server..."
/usr/sbin/cron &

# start nginx
nginx -g "daemon off;"
